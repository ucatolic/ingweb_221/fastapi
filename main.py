# Python
from typing import Optional, List, Dict

# Pydantic
from pydantic import BaseModel, Field

# FastAPI
from fastapi import FastAPI
from fastapi import Query, Path, HTTPException, status, Body
from fastapi.encoders import jsonable_encoder
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

# Project
from database import cars

#templates = Jinja2Templates(directory="templates")


# Models
class Car(BaseModel):
    make: Optional[str]
    model: Optional[str]
    year: Optional [int] = Field(..., ge=1980, lt=2022)
    price: Optional[float]
    engine: Optional[str]="V4"
    autonomous: Optional[bool]
    sold: Optional[List[str]]


app = FastAPI()
#app.mount("/static", StaticFiles(directory="static"), name="static")

@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get(path="/cars",
         response_model=List[Dict[str, Car]],
         tags=["Get"],
         summary="List all cars",
         status_code=status.HTTP_200_OK
         )
def get_cars(number: Optional[str] = Query("10", max_length=3)):
    response = []
    for id, car in list(cars.items())[:int(number)]:
        to_add = {}
        to_add[id] = car
        response.append(to_add)
    return response
#Could be query





@app.get(path="/cars/{id}",
         response_model=Car,
         status_code=status.HTTP_200_OK,
         tags=["Cars"],
         summary="Find car by Id")
def get_car_by_id(id: int = Path(..., ge=0, lt=1000)):
    car = cars.get(id)
    if not car:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No car found")
    return car







@app.post(path="/cars",
          status_code=status.HTTP_201_CREATED,
          tags=["Cars"],
          summary="Add a car")
def add_cars(body_cars: List[Car], min_id: Optional[int]= Body(0)):
    if len(body_cars) < 1:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="No cars to add")
    min_id = len(cars.values())+min_id
    for car in body_cars:
        while cars.get(min_id):
            min_id+=1
        cars[min_id]=car
        min_id+=1








@app.put(path="/cars/{id}",
         response_model=Dict[str, Car],
         status_code=status.HTTP_202_ACCEPTED,
         tags=["Cars"],
         summary="Updated a car")
def update_car(id: int, car: Car=Body(...)):
    stored = cars.get(id)
    if not stored:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Car not found")
    stored = Car(**stored)
    new = car.dict(exclude_unset=True)
    new = stored.copy(update=new)
    cars[id] = jsonable_encoder(new)
    response = {}
    response[id] = cars[id]
    return response






@app.delete(path="/cars/{id}",
            status_code=status.HTTP_205_RESET_CONTENT,
            tags=["Cars"],
            summary="Remove a car from the database")
def delete_car(id:int):
    if not cars.get(id):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Car not found to be delete")
    del cars[id]









